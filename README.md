# BoletoFacil #
![logo ](https://www.boletobancario.com/img/boleto-bancario-logo.png "Logo")

Geração de boletos através do boletobancario.com

[Link da documentação oficial](https://sandbox.boletobancario.com/boletofacil/integration/integration.html)

Desenvolvido em:
* .net C#
* Framework 4.5.2
* Newtonsoft.Json

---
## Como utilizar ##

* Class library toda desenvolvida em TDD
* Utilizes os testes para entender a documentação
* Após modificações rode os testes

## E se eu quiser criar novas funções? ##

* Crie um teste básico que seja para o sua função
* Desejado: Testes completos


### Exemplos de usos ###

#### criado cobrança ####
```cs
var cobranca = new Cobranca()
    {
        amount = 12.75M,
        description = "Pedido1791",
        payerName = "NomeSeuCliente",
        payerCpfCnpj = "94648945123"
    };
var novoBoleto = new BoletoFacil.Boleto();
var retorno = novoBoleto.CriarBoleto(cobranca);

if(retorno.sucess){
    Console.Write("sucesso");
}

```
Cobrança completa
```cs
var cobranca = new Cobranca()
    {
        amount = 12.75M,
        description = "Pedido1791",
        payerName = "NomeSeuCliente",
        payerCpfCnpj = "94648945123",
        discountAmount = 10M,
        discountDays = 1,
        fine = 10M,
        installments = 2,
        interest = 5.4M,
        maxOverdueDays = 1,
        notificationUrl = "http://meusite.com.br/webhook",
        notifyPayer = true,
        payerBirthDate = new DateTime(1981, 02, 02),
        payerEmail = "leonardo99885566@gmail.com",
        payerPhone = "11996908255",
        payerSecondaryEmail = "zecaurubu998877@gmail.com",
        reference = "AB999",
    };
var novoBoleto = new BoletoFacil.Boleto();
var retorno = novoBoleto.CriarBoleto(cobranca);

if(retorno.sucess){
    Console.Write("sucesso");
}

```
---
#### cancelando cobrança ####
```cs
//gerando uma cobrança para poder ser cancelada
var cobranca = new Cobranca()
    {
        amount = 12.75M,
        description = "Pedido1791",
        payerName = "NomeSeuCliente",
        payerCpfCnpj = "94648945123"
    };

var novoBoleto = new BoletoFacil.Boleto();
var retorno = novoBoleto.CriarBoleto(cobranca);
// da nova cobrança fim

var codigoBoleto = retorno.data.charges[0].code; //recupera código do boleto gerado

var retornoCancelamento = novoBoleto.CancelarBoleto(codigoBoleto);
```
---
#### Consultando Cobrança ####
```cs
var paymentToken = "29A1DF4272C4E92B990F6752AD29FDC1";
var pag = new Consultas();
var retorno = pag.ConsultarCobranca(paymentToken);
```
---
#### Consultando Saldo ####
```cs
var pag = new Consultas();
var retorno = pag.Saldo();
```