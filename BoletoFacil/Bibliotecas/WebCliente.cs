using BoletoFacil.Models;
using System.IO;
using System.Net;

namespace BoletoFacil
{
    public class WebCliente
    {
        public static RetornoWebCliente WebRequest(string urlfinal)
        {
            try
            {
                var requisicaoWeb = System.Net.WebRequest.CreateHttp(urlfinal);
                requisicaoWeb.Method = "GET";
                requisicaoWeb.UserAgent = "RequisicaoBoleto";

                using (var resposta = (HttpWebResponse)requisicaoWeb.GetResponse())
                {
                    if (resposta.StatusCode == HttpStatusCode.OK)
                    {
                        var streamDados = resposta.GetResponseStream();
                        StreamReader reader = new StreamReader(streamDados);
                        object objResponse = reader.ReadToEnd();

                        streamDados.Close();
                        resposta.Close();
                        var retorno = new RetornoWebCliente(objResponse.ToString());

                        return retorno;
                    }
                    else
                    {
                        var retorno = new RetornoWebCliente();
                        retorno.ErrorMessage.success = false;
                        retorno.ErrorMessage.errorMessage = "erro";
                        return retorno;
                    }
                }
            }
            catch (System.Exception ex)
            {
                var retorno = new RetornoWebCliente();
                retorno.ErrorMessage.success = false;
                retorno.ErrorMessage.errorMessage = ex.Message;
                return retorno;
            }
        }
    }
}
