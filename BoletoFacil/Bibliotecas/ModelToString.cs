using BoletoFacil.Models;
using BoletoFacil.Models.Enums;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System;

namespace BoletoFacil
{
    public class ModelToString
    {
        public static string MontaUrlCobranca(Cobranca objeto)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            var txt = new StringBuilder();

            txt.Append(Configs.Url + "issue-charge");
            txt.Append("?");

            foreach (var p in objeto.GetType().GetProperties().Where(p => p.GetGetMethod().GetParameters().Count() == 0))
            {
                if (p.PropertyType == typeof(DateTime?))
                {
                    var valor =p.GetValue(objeto, null);
                    if (valor != null)
                    {
                        var data = Convert.ToDateTime(valor);
                        txt.Append(p.Name + "=" + data.ToShortDateString() + "&");
                    }
                }
                else
                {
                    var valor = p.GetValue(objeto, null);
                    if (valor != null)
                    {
                        txt.Append(p.Name + "=" + p.GetValue(objeto, null) + "&");
                    }
                }

            }

            return txt.ToString();
        }

        internal static string MontaUrlConsultaSaldo(TipoResponseType tipoRetorno = TipoResponseType.JSON)
        {
            var txt = new StringBuilder();
            txt.Append(Configs.Url + "fetch-balance");
            txt.Append("?token=" + Configs.Token);
            txt.Append("&responseType=" + tipoRetorno);
            return txt.ToString();
        }

        public static string MontaUrlCancelamento(int code, TipoResponseType tipoRetorno = TipoResponseType.JSON)
        {
            var txt = new StringBuilder();
            txt.Append(Configs.Url + "cancel-charge");
            txt.Append("?token=" + Configs.Token);
            txt.Append("&code=" + code);
            txt.Append("&responseType=" + tipoRetorno);
            return txt.ToString();
        }

        public static string MontaUrlConsultaPagamento(string paymentToken, TipoResponseType tipoRetorno = TipoResponseType.JSON)
        {
            var txt = new StringBuilder();
            txt.Append(Configs.Url + "fetch-payment-details");
            txt.Append("?paymentToken=" + paymentToken);
            txt.Append("&responseType=" + tipoRetorno);
            return txt.ToString();
        }
    }
}
