using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoletoFacil.Models
{
    public class RetornoPagamento
    {
        public DataPagamento data { get; set; }
        public bool success { get; set; }

        public string errorMessage { get; set; }
    }

    public class DataPagamento
    {
        public Payment payment { get; set; }
    }

    public class Payment
    {
        public int id { get; set; }
        public decimal amount { get; set; }
        public ChargePagamento charge { get; set; }
        public string date { get; set; }
        public decimal fee { get; set; }
        public string status { get; set; }
        public string type { get; set; }
    }

    public class ChargePagamento
    {
        public decimal amount { get; set; }
        public int code { get; set; }
        public string dueDate { get; set; }
        public string reference { get; set; }
    }

}
