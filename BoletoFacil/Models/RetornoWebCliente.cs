using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoletoFacil.Models
{
    /// <summary>
    /// Retorno genérico que é utilizado no webcliente
    /// </summary>
    public class RetornoWebCliente
    {
        public RetornoWebCliente()
        {
            ErrorMessage = new ErrorMessage();

        }
        public RetornoWebCliente(string valor)
        {
            RetornoJson = valor;
            ErrorMessage = new ErrorMessage();
        }
        public string RetornoJson { get; set; }

        public ErrorMessage ErrorMessage { get; set; }
    }
}
