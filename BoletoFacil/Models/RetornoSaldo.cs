using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoletoFacil.Models
{


    public class RetornoSaldo
    {
        public DataSaldo data { get; set; }
        public bool success { get; set; }
        public string errorMessage { get; set; }
    }

    public class DataSaldo
    {
        public float balance { get; set; }
        public int withheldBalance { get; set; }
        public float transferableBalance { get; set; }
    }

}
