using BoletoFacil.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace BoletoFacil.Models
{
    //https://sandbox.boletobancario.com/boletofacil/integration/integration.html

    public class Cobranca
    {
        [Required]
        public string token { get; set; } = Configs.Token;
        [Required]
        public string description { get; set; }

        [StringLength(255)]
        public string reference { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:#.####}")]
        public decimal amount { get; set; }


        /// <summary>
        /// Data de vencimento do boleto ou da primeira parcela, no caso de cobrança parcelada
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dueDate { get; set; }

        /// <summary>
        /// Número de parcelas da cobrança, default=1
        /// </summary>
        public int? installments { get; set; }


        /// <summary>
        /// Número máximo de dias que o boleto poderá ser pago após o vencimento,
        /// Zero significa que o boleto não poderá ser pago após o vencimento
        /// Default: 0
        /// </summary>
        public int? maxOverdueDays { get; set; }

        /// <summary>
        /// Multa para pagamento após o vencimento
        /// Só é efetivo se maxOverdueDays for maior que zero
        /// </summary>
        public decimal? fine { get; set; }


        /// <summary>
        /// Juros para pagamento após o vencimento
        /// Só é efetivo se maxOverdueDays for maior que zero
        /// </summary>
        public decimal? interest { get; set; }


        /// <summary>
        /// Valor do desconto
        /// </summary>
        public decimal? discountAmount { get; set; }

        /// <summary>
        /// Dias concessão de desconto para pagamento antecipado. Exemplo: Até 10 dias antes do vencimento.
        /// Valor igual a 0 (zero) significa conceder desconto até a data do vencimento
        /// </summary>
        public int? discountDays { get; set; }



        /// <summary>
        /// Nome completo do comprador
        /// </summary>
        [Required]
        [StringLength(80)]
        public string payerName { get; set; }

        /// <summary>
        /// CPF ou CNPJ do comprador
        /// </summary>
        [Required]
        public string payerCpfCnpj { get; set; }


        /// <summary>
        /// Endereço de email do comprador
        /// </summary>
        [StringLength(80)]
        public string payerEmail { get; set; }

        /// <summary>
        /// Endereço de email secundário do comprador
        /// </summary>
        [StringLength(80)]
        public string payerSecondaryEmail { get; set; }

        /// <summary>
        /// Telefone do comprador
        /// </summary>
        [StringLength(25)]
        public string payerPhone { get; set; }

        /// <summary>
        /// Data de nascimento do comprador
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? payerBirthDate { get; set; }

        /// <summary>
        /// Define se o Boleto Fácil enviará emails de notificação sobre esta cobrança para o comprador
        /// O email com o boleto ou carnê só será enviado ao comprador se este parâmetro for igual a true e o endereço de email do comprador estiver presente
        /// O lembrete de vencimento só será enviado se as condições acima forem verdadeiras e se na configuração do Favorecido os lembretes estiverem ativados
        /// Default: true
        /// </summary>
        public bool? notifyPayer { get; set; }

        /// <summary>
        /// Define uma URL de notificação para que o Boleto Fácil envie notificações ao seu sistema sempre que houver algum evento de pagamento desta cobrança.
        /// Se preferir, você pode configurar uma URL de notificação para todas as suas cobranças no tópico Notificação de pagamentos.
        /// </summary>
        [StringLength(255)]
        public string notificationUrl { get; set; }

        public TipoResponseType? responseType { get; set; }
        
    }
}
