namespace BoletoFacil.Models
{

    public class RetornoCobranca
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public string errorMessage { get; set; }
    }

    public class Data
    {
        public Charge[] charges { get; set; }
    }

    public class Charge
    {
        public int code { get; set; }
        public string dueDate { get; set; }
        public string link { get; set; }
        public string payNumber { get; set; }
    }
}
