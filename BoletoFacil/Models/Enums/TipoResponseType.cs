namespace BoletoFacil.Models.Enums
{
    public enum TipoResponseType
    {
       // [Description("JSON")]
        JSON,
       // [Description("XML")]
        XML
    }

    //public static class MyEnumExtensions
    //{
    //    public static string ToDescriptionString(this TipoResponseType val)
    //    {
    //        DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
    //        return attributes.Length > 0 ? attributes[0].Description : string.Empty;
    //    }
    //}
}
