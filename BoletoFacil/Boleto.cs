using System;
using BoletoFacil.Models;
using Newtonsoft.Json;
using System.Text;
using BoletoFacil.Models.Enums;

namespace BoletoFacil
{
    public class Boleto : IBoleto
    {
        public RetornoCancelar CancelarBoleto(int code, TipoResponseType tipoRetorno = TipoResponseType.JSON)
        {
            var urlFinal = ModelToString.MontaUrlCancelamento(code, tipoRetorno);

            var retorno = WebCliente.WebRequest(urlFinal);
            if (retorno.ErrorMessage.errorMessage == null)
            {
                return JsonConvert.DeserializeObject<RetornoCancelar>(retorno.RetornoJson);
            }
            else
            {
                return new RetornoCancelar() { success = false, data = retorno.ErrorMessage.errorMessage };
            }

        }



        public RetornoCobranca CriarBoleto(Cobranca valor)
        {
            var urlfinal = ModelToString.MontaUrlCobranca(valor);

            var retorno = WebCliente.WebRequest(urlfinal);
            if (retorno.ErrorMessage.errorMessage == null)
            {
                return JsonConvert.DeserializeObject<RetornoCobranca>(retorno.RetornoJson);
            }
            else
            {
                return new RetornoCobranca() { success = false, errorMessage = retorno.ErrorMessage.errorMessage };
            }
        }


    }
}
