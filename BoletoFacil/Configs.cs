namespace BoletoFacil
{
    public class Configs
    {
        //SandBox //gere seu token de sandbox em:
        //https://sandbox.boletobancario.com/boletofacil/integration/integration.html

        public static readonly string Token = "FC37BC8BF112E396BA25BF7D4E1271594640CC5A9E89A56DCA744016EE428C51";
        public static readonly string Url = "https://sandbox.boletobancario.com/boletofacil/integration/api/v1/";
        public static readonly string notificationUrl = "";


        //Produção
        //public static readonly string Token = "__Gere_seu_toekn___";
        //public static readonly string Url = "https://boletobancario.com/boletofacil/integration/api/v1/";
        //public static readonly string notificationUrl = "http://seusite.com.br/feedback";

    }
}
