using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoletoFacil.Models;
using Newtonsoft.Json;

namespace BoletoFacil
{
    public class Consultas: IConsultas
    {
        public  RetornoPagamento ConsultarCobranca(string valor)
        {
            var urlFinal = ModelToString.MontaUrlConsultaPagamento(valor);

            var retorno = WebCliente.WebRequest(urlFinal);
            if (retorno.ErrorMessage.errorMessage == null)
            {
                return JsonConvert.DeserializeObject<RetornoPagamento>(retorno.RetornoJson);
            }
            else
            {
                return new RetornoPagamento() { success = false, errorMessage = retorno.ErrorMessage.errorMessage };
            }

        }

        public RetornoSaldo Saldo()
        {
            var urlFinal = ModelToString.MontaUrlConsultaSaldo();
            var retorno = WebCliente.WebRequest(urlFinal);
            if (retorno.ErrorMessage.errorMessage == null)
            {
                return JsonConvert.DeserializeObject<RetornoSaldo>(retorno.RetornoJson);
            }
            else
            {
                return new RetornoSaldo() { success = false, errorMessage = retorno.ErrorMessage.errorMessage };
            }
        }
    }


}
