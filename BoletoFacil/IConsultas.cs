using BoletoFacil.Models;

namespace BoletoFacil
{
    public interface IConsultas
    {
        RetornoPagamento ConsultarCobranca(string valor);

        RetornoSaldo Saldo();
    }
}