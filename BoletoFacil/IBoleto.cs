using BoletoFacil.Models;
using BoletoFacil.Models.Enums;

namespace BoletoFacil
{
    public interface IBoleto
    {
        RetornoCobranca CriarBoleto(Cobranca valor);

        RetornoCancelar CancelarBoleto(int code, TipoResponseType tipoRetorno = TipoResponseType.JSON);
    }
}
