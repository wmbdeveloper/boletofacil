using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExemploMVC.Models;
using BoletoFacil.Models;

namespace ExemploMVC.Controllers
{
    public class BoletosController : Controller
    {
        private readonly ExemploMVCContext db = new ExemploMVCContext();


        public async Task<ActionResult> GerarBoleto(int id)
        {
            //recuperando informações
            Boleto boleto = await db.Boletos.FindAsync(id);

            //gerando cobrança BoletoFacil
            var bol = new Cobranca()
            {
                amount = boleto.Valor,
                description = "Teste",
                payerName = boleto.Cliente.Nome,
                payerCpfCnpj = boleto.Cliente.CpfCnpj
            };
            var novoBoleto = new BoletoFacil.Boleto();
            var retorno = novoBoleto.CriarBoleto(bol);

            return View(retorno);
        }

        public async Task<ActionResult> Index()
        {
            var boletos = db.Boletos.Include(b => b.Cliente);
            return View(await boletos.ToListAsync());
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boleto boleto = await db.Boletos.FindAsync(id);
            if (boleto == null)
            {
                return HttpNotFound();
            }
            return View(boleto);
        }

        public ActionResult Create()
        {
            ViewBag.ClienteId = new SelectList(db.Clientes.OrderBy(o => o.Nome), "ClienteId", "Nome");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BoletoId,ClienteId,Valor,Vencimento")] Boleto boleto)
        {
            if (ModelState.IsValid)
            {
                db.Boletos.Add(boleto);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ClienteId = new SelectList(db.Clientes.OrderBy(o => o.Nome), "ClienteId", "Nome", boleto.ClienteId);
            return View(boleto);
        }

        // GET: Boletoes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boleto boleto = await db.Boletos.FindAsync(id);
            if (boleto == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClienteId = new SelectList(db.Clientes.OrderBy(o => o.Nome), "ClienteId", "Nome", boleto.ClienteId);
            return View(boleto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "BoletoId,ClienteId,Valor,Vencimento")] Boleto boleto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(boleto).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ClienteId = new SelectList(db.Clientes.OrderBy(o => o.Nome), "ClienteId", "Nome", boleto.ClienteId);
            return View(boleto);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boleto boleto = await db.Boletos.FindAsync(id);
            if (boleto == null)
            {
                return HttpNotFound();
            }
            return View(boleto);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Boleto boleto = await db.Boletos.FindAsync(id);
            db.Boletos.Remove(boleto);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
