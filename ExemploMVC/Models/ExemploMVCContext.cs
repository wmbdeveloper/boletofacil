using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ExemploMVC.Models
{
    public class ExemploMVCContext : DbContext
    {
        public ExemploMVCContext() 
            : base("DefaultConnection")
        {

        }

        public DbSet<Boleto> Boletos { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
    }
}