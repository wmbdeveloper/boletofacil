using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExemploMVC.Models
{
    public class Cliente
    {

        public int ClienteId { get; set; }

        [MaxLength(200)]
        public string Nome { get; set; }

        [MaxLength(200)]
        public string CpfCnpj { get; set; }

        public virtual IEnumerable<Boleto> Boletos { get; set; }
    }
}