using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExemploMVC.Models
{
    public class Boleto
    {
        public int BoletoId { get; set; }

        public int ClienteId { get; set; }

        public decimal Valor { get; set; }

        public DateTime Vencimento { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}