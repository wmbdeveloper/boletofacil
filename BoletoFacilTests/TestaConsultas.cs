using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BoletoFacil;

namespace BoletoFacilTests
{
    [TestClass]
    public class Consultar
    {
        [TestMethod]
        public void ConsultarPagamento()
        {
            var paymentToken = "29A1DF4272C4E92B990F6752AD29FDC9";

            var pag = new Consultas();
            var retorno = pag.ConsultarCobranca(paymentToken);

            Assert.AreEqual(true, retorno.success);
        }

        [TestMethod]
        public void ConsultarPagamentoTokenInvalido()
        {
            var paymentToken = "29A1DF4272C4E92B990F6752AD29FDC0";

            var pag = new Consultas();
            var retorno = pag.ConsultarCobranca(paymentToken);

            Assert.AreEqual(false, retorno.success);
        }

        [TestMethod]
        public void ConsultarSaldo()
        {
            var pag = new Consultas();
            var retorno = pag.Saldo();

            Assert.AreEqual(true, retorno.success);
        }
    }
}
