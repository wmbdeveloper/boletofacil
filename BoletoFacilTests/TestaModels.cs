using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BoletoFacil.Models;

namespace BoletoFacilTests
{
    [TestClass]
    public class TestaModels
    {
        [TestMethod]
        public void TestaEnumResponseTypeJSON()
        {

            var bol = new Cobranca()
            {
                amount = 1M,
                description = "p",
                payerName = "n",
                payerCpfCnpj = "94648945123",
                responseType = BoletoFacil.Models.Enums.TipoResponseType.JSON
            };

            var valorEsperado = "JSON";

            Assert.AreEqual(valorEsperado, bol.responseType.ToString());
        }
        [TestMethod]
        public void TestaEnumResponseTypeXML()
        {

            var bol = new Cobranca()
            {
                amount = 1M,
                description = "p",
                payerName = "n",
                payerCpfCnpj = "94648945123",
                responseType = BoletoFacil.Models.Enums.TipoResponseType.XML
            };

            var valorEsperado = "XML";

            Assert.AreEqual(valorEsperado, bol.responseType.ToString());
        }
    }
}
