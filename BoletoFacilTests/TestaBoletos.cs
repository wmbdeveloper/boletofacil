using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BoletoFacil.Models;

namespace BoletoFacilTests
{
    [TestClass]
    public class TestaBoletos
    {
        public Cobranca GerarCobrancaPadrao() {
            var bol = new Cobranca()
            {
                amount = 120.75M,
                description = "Pedido1791",
                payerName = "teste",
                payerCpfCnpj = "94648945123"
            };

            return bol;
        }



        [TestMethod]
        public void CriarBoletoBasico()
        {
            var cobranca =  GerarCobrancaPadrao();

            var novoBoleto = new BoletoFacil.Boleto();
            var retorno = novoBoleto.CriarBoleto(cobranca);

            Assert.AreEqual(true, retorno.success);
        }

        [TestMethod]
        public void CriarBoletoCompleto()
        {
            var cobranca = GerarCobrancaPadrao();
            cobranca.discountAmount = 10M;
            cobranca.discountDays = 1;
            cobranca.fine = 10M;
            cobranca.installments = 2;
            cobranca.interest = 5.4M;
            cobranca.maxOverdueDays = 1;
            cobranca.notificationUrl = "http://meusite.com.br/webhook";
            cobranca.notifyPayer = true;
            cobranca.payerBirthDate = new DateTime(1981, 02, 02);
            cobranca.payerEmail = "leonardo99885566@gmail.com";
            cobranca.payerPhone = "11996908255";
            cobranca.payerSecondaryEmail = "zecaurubu998877@gmail.com";
            cobranca.reference = "AB999";

            var novoBoleto = new BoletoFacil.Boleto();
            var retorno = novoBoleto.CriarBoleto(cobranca);

            Assert.AreEqual(true, retorno.success);
        }
        [TestMethod]
        public void CriarBoleto1Real()
        {
            var cobranca = GerarCobrancaPadrao();
            cobranca.amount = 1M;

            var novoBoleto = new BoletoFacil.Boleto();
            var retorno = novoBoleto.CriarBoleto(cobranca);

            Assert.AreEqual(false, retorno.success);
        }
        [TestMethod]
        public void CancelarBoletoBasico()
        {
            var cobranca = GerarCobrancaPadrao();

            var novoBoleto = new BoletoFacil.Boleto();
            var code = novoBoleto.CriarBoleto(cobranca).data.charges[0].code;

            var retorno = novoBoleto.CancelarBoleto(code);

            Assert.AreEqual(true, retorno.success);
        }
    }
}
