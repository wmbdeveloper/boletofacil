using Microsoft.VisualStudio.TestTools.UnitTesting;
using BoletoFacil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoletoFacil.Models;

namespace BoletoFacil.Tests
{
    [TestClass()]
    public class TestUrl
    {

        public void TestaUrlCancelmento()
        {
            Random rand = new Random();
            var code = rand.Next(0, 200);

            var urlEsperada = $"https://sandbox.boletobancario.com/boletofacil/integration/api/v1/cancel-charge?token=token&code={code}&responseType=JSON";
            var urlfinal = ModelToString.MontaUrlCancelamento(code, Models.Enums.TipoResponseType.JSON);

            Assert.AreEqual(urlEsperada, urlfinal);
        }
        [TestMethod()]
        /*Testa url e também o sistema Boleto e token*/
        public void TestaAlgunsParametrosUrl()
        {
            var bol = new Cobranca()
            {
                amount = 12.75M,
                description = "Pedido1791",
                payerName = "teste",
                payerCpfCnpj = "94648945123"
            };
            var urlfinal = ModelToString.MontaUrlCobranca(bol);


            var urlCorreta = "https://sandbox.boletobancario.com/boletofacil/integration/api/v1/issue-charge?token=FC37BC8BF112E396BA25BF7D4E1271594640CC5A9E89A56DCA744016EE428C51&description=Pedido1791&amount=12.75&payerName=teste&payerCpfCnpj=94648945123&";
            Assert.AreEqual(urlCorreta, urlfinal);
        }

        [TestMethod()]
        public void TestaTodosParametrosUrl()
        {

            var bol = new Cobranca()
            {
                token = "token",
                amount = 12.75M,
                description = "Pedido1791",
                payerName = "teste",
                payerCpfCnpj = "94648945123",
                discountAmount = 10,
                discountDays = 9,
                installments = 4,
                notifyPayer = true,
                notificationUrl = "http://localhost:8000",
                responseType = Models.Enums.TipoResponseType.XML
            };
            var urlfinal = ModelToString.MontaUrlCobranca(bol);

            var txt = new StringBuilder();
            txt.Append("https://sandbox.boletobancario.com/boletofacil/integration/api/v1/issue-charge");
                txt.Append("?token=token");
                txt.Append("&description=Pedido1791");
                txt.Append("&amount=12.75");
                txt.Append("&installments=4");
                txt.Append("&discountAmount=10");
                txt.Append("&discountDays=9");
                txt.Append("&payerName=teste");
                txt.Append("&payerCpfCnpj=94648945123");
                txt.Append("&notifyPayer=True");
                txt.Append("&notificationUrl=http://localhost:8000");
                txt.Append("&responseType=XML");
                txt.Append("&");

            Assert.AreEqual(txt.ToString(), urlfinal);
        }
    }
}